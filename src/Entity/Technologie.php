<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TechnologieRepository")
 */
class Technologie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $html;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CSS;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Javascript;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PHP;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Symfony;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Bootstrap;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function setHtml(string $html): self
    {
        $this->html = $html;

        return $this;
    }

    public function getCSS(): ?string
    {
        return $this->CSS;
    }

    public function setCSS(string $CSS): self
    {
        $this->CSS = $CSS;

        return $this;
    }

    public function getJavascript(): ?string
    {
        return $this->Javascript;
    }

    public function setJavascript(string $Javascript): self
    {
        $this->Javascript = $Javascript;

        return $this;
    }

    public function getPHP(): ?string
    {
        return $this->PHP;
    }

    public function setPHP(string $PHP): self
    {
        $this->PHP = $PHP;

        return $this;
    }

    public function getSymfony(): ?string
    {
        return $this->Symfony;
    }

    public function setSymfony(string $Symfony): self
    {
        $this->Symfony = $Symfony;

        return $this;
    }

    public function getBootstrap(): ?string
    {
        return $this->Bootstrap;
    }

    public function setBootstrap(string $Bootstrap): self
    {
        $this->Bootstrap = $Bootstrap;

        return $this;
    }
}
