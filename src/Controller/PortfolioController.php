<?php

namespace App\Controller;

use Twig\Environment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Project;
use App\Form\ProjectType;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;


class PortfolioController extends AbstractController
{
    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function index()
    {
        $projet = $this->getDoctrine()->getRepository(Project::class);
        $projets = $projet->findAll();
        return $this->render('portfolio/index.html.twig', [
            'controller_name' => 'PortfolioController',
            'projets' => $projets
            
        ]);
    }
    /**
     * @Route("/", name="home")
     */

    public function home (){
        
        return $this->render('portfolio/home.html.twig'); 

                
    }

    /**
     * @Route("/new", name="new")
     */
    
    public function newProject(Request $request){
        $project = new Project();

        $form = $this->createForm(ProjectType::class, $project); 

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();
            return $this->redirectToRoute('portfolio');
        }
        return $this->render('portfolio/newprojet.html.twig',[
            'formProject' => $form->createView(),
            ]);
    }

}
